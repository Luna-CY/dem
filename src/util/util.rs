pub(crate) mod network {
    pub(crate) mod network;
    pub(crate) mod downloader;
}

pub(crate) mod archive {
    pub(crate) mod zip;
    pub(crate) mod gzip;
    pub(crate) mod xz;
}

pub(crate) mod reader {
    pub(crate) mod proxy;
}

pub mod system {
    pub mod syscall;
    pub mod lockpath;
    pub(crate) mod fs;
}

pub(crate) mod installer {
    pub(crate) mod installer;
}