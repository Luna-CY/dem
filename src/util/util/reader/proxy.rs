use std::io::{Read, Result, Seek, SeekFrom};

/// 代理Read访问
pub struct Proxy<R, F: Fn(usize)> {
    source: R,
    callback: F,
}

impl<R, F: Fn(usize)> Proxy<R, F> {
    pub fn new(source: R, callback: F) -> Proxy<R, F> {
        return Proxy { source, callback };
    }
}

impl<R, F: Fn(usize)> Read for Proxy<R, F> where R: Read {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize> {
        let us = self.source.read(buf)?;
        (self.callback)(us);

        Ok(us)
    }
}

impl<R, F: Fn(usize)> Seek for Proxy<R, F> where R: Seek {
    fn seek(&mut self, pos: SeekFrom) -> Result<u64> {
        self.source.seek(pos)
    }
}