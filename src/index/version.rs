use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, Default, Clone)]
pub struct Scripts {
    // 操作前执行脚本
    #[serde(default)]
    pub before: Vec<String>,
    // 操作后执行脚本
    #[serde(default)]
    pub after: Vec<String>,
}

#[derive(Debug, Serialize, Deserialize, Default, Clone)]
pub struct Script {
    // 安装脚本
    #[serde(default)]
    pub install: Scripts,
    // 移除脚本
    #[serde(default)]
    pub remove: Scripts,
}

#[derive(Debug, Serialize, Deserialize, Default, Clone)]
pub struct Archive {
    // 是否启用预构建安装
    #[serde(default)]
    pub enable: bool,
    // 包地址
    #[serde(default)]
    pub package: String,
    // 脚本配置
    #[serde(default)]
    pub script: Script,
}

#[derive(Debug, Serialize, Deserialize, Default, Clone)]
pub struct Build {
    // 操作链
    #[serde(default)]
    pub chains: Vec<String>,
}

#[derive(Debug, Serialize, Deserialize, Default, Clone)]
pub struct Source {
    // 是否启用预构建安装
    #[serde(default)]
    pub enable: bool,
    // 包地址
    #[serde(default)]
    pub package: String,
    // 编译配置项
    #[serde(default)]
    pub build: Build,
    // 依赖
    #[serde(default)]
    pub depends: Vec<Version>,
}

#[derive(Debug, Serialize, Deserialize, Default, Clone)]
pub struct Version {
    // 版本号
    pub version: String,
    // 搜索路径表
    #[serde(default)]
    pub paths: Vec<String>,
    // 环境变量表
    #[serde(default)]
    pub environments: Vec<String>,
    // 预构建包安装
    #[serde(default)]
    pub archive: Archive,
    // 源码编译安装
    #[serde(default)]
    pub source: Source,
}
