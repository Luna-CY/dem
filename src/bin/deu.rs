use std::fs;
use std::path::Path;

use clap::Command;

use dem::command::command;
use dem::runtime::runtime::{path, version};

fn main() {
    let mut app = Command::new("deu").version(version::VERSION_NAME).about("辅助工具，用于管理当前开发环境配置");
    app = app.subcommand(command::environment::new());
    app = app.subcommand(command::index::new());
    app = app.subcommand(command::install::new());
    app = app.subcommand(command::remove::new());
    app = app.subcommand(command::project::new());
    app = app.subcommand(command::link::new());
    let args = app.clone().get_matches();

    // 初始化索引目录
    let index = Path::new(path::DEM_INDEX_PATH);
    if !index.exists() && fs::create_dir_all(index).is_err() {
        eprintln!("创建索引目录[{}]失败", index.to_str().unwrap());

        return;
    }

    match args.subcommand() {
        Some((command::environment::NAME, subcommand)) => command::environment::action(subcommand),
        Some((command::index::NAME, subcommand)) => command::index::action(subcommand),
        Some((command::install::NAME, subcommand)) => command::install::action(subcommand),
        Some((command::remove::NAME, subcommand)) => command::remove::action(subcommand),
        Some((command::project::NAME, subcommand)) => command::project::action(subcommand),
        Some((command::link::NAME, subcommand)) => command::link::action(subcommand),
        Some(_) => app.print_help().expect("打印帮助信息失败"),
        None => app.print_help().expect("打印帮助信息失败"),
    }
}
