use std::path::Path;

/// 工具的根目录
pub const DEM_ROOT_PATH: &str = "/opt/dem";

/// 索引文件夹名
pub const DEM_INDEX_PATH: &str = "/opt/dem/index";

/// 软件文件夹名
pub const DEM_SOFTWARE_PATH: &str = "/opt/dem/software";

/// 通用替换代码
pub fn replace_package_tokens<P: AsRef<Path>>(package: &str, root: P, version: &str, temp: Option<&str>) -> String {
    let mut package = package.replace("{ROOT}", root.as_ref().to_str().unwrap()).replace("{VERSION}", version);

    let depend = root.as_ref().join("depends");
    package = package.replace("{DEPEND}", depend.to_str().unwrap());

    if temp.is_some() {
        package = package.replace("{TEMP}", temp.unwrap());
    }

    package
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_replace_package_tokens() {
        assert_eq!(super::replace_package_tokens("{ROOT}", "/opt/dem", "1.0.0", None), "/opt/dem");
        assert_eq!(super::replace_package_tokens("{DEPEND}", "/opt/dem", "1.0.0", None), "/opt/dem/depends");
        assert_eq!(super::replace_package_tokens("{VERSION}", "/opt/dem", "1.0.0", None), "1.0.0");
        assert_eq!(super::replace_package_tokens("{TEMP}", "/opt/dem", "1.0.0", Some("/temp")), "/temp");
    }
}