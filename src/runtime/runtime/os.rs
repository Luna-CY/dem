#[cfg(target_os = "macos")]
pub static OS: &'static str = "darwin";

#[cfg(target_os = "linux")]
pub static OS: &'static str = "linux";

#[cfg(target_arch = "x86")]
pub static ARCH: &'static str = "amd";

#[cfg(target_arch = "x86_64")]
pub static ARCH: &'static str = "amd64";

#[cfg(target_arch = "aarch")]
pub static ARCH: &'static str = "arm";

#[cfg(target_arch = "aarch64")]
pub static ARCH: &'static str = "arm64";