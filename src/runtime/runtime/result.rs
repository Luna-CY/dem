/// 通用错误响应
pub type ErrorResult<T> = Result<T, super::error::Error>;

/// Panic错误响应
pub type PanicErrorResult<T> = Result<T, super::error::PanicError>;

/// 内部错误响应
pub type InternalErrorResult<T> = Result<T, super::error::InternalError>;
