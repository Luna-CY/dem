use std::fmt::Formatter;

/// 枚举错误类型
#[derive(Debug)]
pub enum Error {
    PanicError(PanicError),
    InternalError(InternalError),
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        return match self {
            Error::PanicError(err) => {
                write!(f, "{}", err.error)
            }
            Error::InternalError(err) => {
                write!(f, "{}", err.error)
            }
        };
    }
}

impl std::error::Error for Error {}

/// 严重错误
#[derive(Debug)]
pub struct PanicError {
    error: String,
}

impl std::fmt::Display for PanicError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.error)
    }
}

impl std::error::Error for PanicError {}

impl PanicError {
    pub fn new(error: String) -> PanicError {
        PanicError { error }
    }
}

/// 内部级错误
#[derive(Debug)]
pub struct InternalError {
    error: String,
}

impl std::fmt::Display for InternalError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.error)
    }
}

impl std::error::Error for InternalError {}

impl InternalError {
    pub fn new(error: String) -> InternalError {
        InternalError { error }
    }
}
