/// 清单地址
pub const MANIFEST: &'static str = "https://gitee.com/Luna-CY/dem-repo/raw/master/{VERSION}/manifest.yaml";

/// 索引文件地址
pub const INDEX: &'static str = "https://gitee.com/Luna-CY/dem-repo/raw/master/{VERSION}/{OS}/{ARCH}/{NAME}";

/// 代理服务器
pub const PROXY: &'static str = "https://ghproxy.com/";