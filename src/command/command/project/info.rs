use std::collections::HashMap;

use clap::{ArgMatches, Command};

use crate::environment::environment;

pub(super) const NAME: &'static str = "inf";

pub(super) fn new() -> Command {
    Command::new(NAME).about("查看当前项目开发环境的汇总信息")
}

/// 执行info命令
pub(super) fn action(_: &ArgMatches) {
    let software_map = environment::get_project_used_software(false);

    let mut software_vec: Vec<(&String, &String)> = software_map.iter().collect();
    software_vec.sort_by(|a, b| a.1.cmp(b.1));

    for (name, version) in software_vec {
        println!("名称: {:<30}版本: {:<30}", name, version);

        let mut environments: HashMap<String, String> = HashMap::new();

        let global_environments = environment::global().get_environments(name);
        if global_environments.is_some() {
            let global_environments = global_environments.unwrap();
            for (k, v) in global_environments.iter() {
                environments.insert(k.clone(), v.clone());
            }
        }

        let project_environments = environment::project().get_environments(name);
        if project_environments.is_some() {
            let project_environments = project_environments.unwrap();
            for (k, v) in project_environments.iter() {
                environments.insert(k.clone(), v.clone());
            }
        }

        for (k, v) in environments.iter() {
            if "DEMNULL" != v.as_str() {
                println!("\t项目ENV: {}={}", k, v);
            }
        }
    }
}
