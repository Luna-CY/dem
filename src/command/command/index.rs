use clap::{ArgMatches, Command};

mod list;
mod info;
mod update;

pub const NAME: &'static str = "ind";

pub fn new() -> Command {
    let command = Command::new(NAME).about("索引管理命令，用于处理与索引相关的操作");

    command.subcommand(list::new()).subcommand(info::new()).subcommand(update::new())
}

pub fn action(command: &ArgMatches) {
    match command.subcommand() {
        Some((list::NAME, subcommand)) => list::action(subcommand),
        Some((info::NAME, subcommand)) => info::action(subcommand),
        Some((update::NAME, subcommand)) => update::action(subcommand),
        Some(_) => new().print_help().expect("打印帮助信息失败"),
        None => new().print_help().expect("打印帮助信息失败"),
    }
}