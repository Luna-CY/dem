use clap::{Arg, ArgMatches, Command};

use crate::environment::environment;
use crate::index::index;

pub(super) const NAME: &'static str = "inf";

pub(super) fn new() -> Command {
    let command = Command::new(NAME).about("查看工具详情");

    command.arg(Arg::new("name"))
}

pub(super) fn action(args: &ArgMatches) {
    let name = args.get_one::<String>("name").expect("No name given");

    let software = index::index().get(name).expect(format!("No software: {}", name).as_str());

    print!("{} ", software.name);
    for version in software.get_versions() {
        print!("{}({}) ", version, if environment::is_installed(name, version) { "✔" } else { "✖" });
    }

    print!("\n");
    println!("{}\n", software.long);
}