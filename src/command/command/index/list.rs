use clap::{ArgMatches, Command};

use crate::environment::environment;
use crate::index::index;

pub(super) const NAME: &'static str = "lis";

/// new
pub(super) fn new() -> Command {
    Command::new(NAME).about("查看索引列表")
}

pub(super) fn action(_: &ArgMatches) {
    let software_list = index::index();

    let mut names = Vec::from_iter(software_list.keys());
    names.sort_by(|a, b| a.cmp(b));

    for name in names {
        let software = software_list.get(name).unwrap();
        let versions = software.get_versions();

        let mut installed: Vec<&str> = Vec::new();
        for version in versions.iter() {
            if environment::is_installed(name, version) {
                installed.push(version);
            }
        }

        if 0 == installed.len() {
            println!("{}(未安装): {}", name, software.short);

            continue;
        }

        println!("{}(已安装:{}): {}", name, installed.join(","), software.short);
    }
}