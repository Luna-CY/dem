use clap::{ArgMatches, Command};

mod set;
mod unset;
mod used;
mod init;
mod info;

pub const NAME: &'static str = "pro";

pub fn new() -> Command {
    let command = Command::new(NAME).about("项目环境管理命令，用于处理与环境相关的操作");

    command.subcommand(set::new()).subcommand(unset::new()).subcommand(used::new()).subcommand(init::new()).subcommand(info::new())
}

pub fn action(command: &ArgMatches) {
    match command.subcommand() {
        Some((set::NAME, subcommand)) => set::action(subcommand),
        Some((unset::NAME, subcommand)) => unset::action(subcommand),
        Some((used::NAME, subcommand)) => used::action(subcommand),
        Some((init::NAME, subcommand)) => init::action(subcommand),
        Some((info::NAME, subcommand)) => info::action(subcommand),
        Some(_) => new().print_help().expect("打印帮助信息失败"),
        None => new().print_help().expect("打印帮助信息失败"),
    }
}
