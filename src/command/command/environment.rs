use clap::{ArgMatches, Command};

mod info;
mod get;
mod set;
mod unset;
mod used;

pub const NAME: &'static str = "env";

/// 创建环境管理子命令并注册次级命令与参数
pub fn new() -> Command {
    let command = Command::new(NAME).about("开发环境管理命令，用于处理与环境相关的操作");

    command.subcommand(info::new()).subcommand(get::new()).subcommand(set::new()).subcommand(unset::new()).subcommand(used::new())
}

/// 执行env命令
pub fn action(command: &ArgMatches) {
    match command.subcommand() {
        Some((info::NAME, subcommand)) => info::action(subcommand),
        Some((get::NAME, subcommand)) => get::action(subcommand),
        Some((set::NAME, subcommand)) => set::action(subcommand),
        Some((unset::NAME, subcommand)) => unset::action(subcommand),
        Some((used::NAME, subcommand)) => used::action(subcommand),
        Some(_) => new().print_help().expect("打印帮助信息失败"),
        None => new().print_help().expect("打印帮助信息失败"),
    }
}