use clap::{ArgMatches, Command};

use crate::environment::environment;

pub(super) const NAME: &'static str = "inf";

/// 创建info命令并注册选项与参数
pub(super) fn new() -> Command {
    Command::new(NAME).about("查看全局开发环境的汇总信息")
}

/// 执行info命令
pub(super) fn action(_: &ArgMatches) {
    let software_map = environment::get_project_used_software(false);

    let mut software_vec: Vec<(&String, &String)> = software_map.iter().collect();
    software_vec.sort_by(|a, b| a.1.cmp(b.1));

    for (name, version) in software_vec {
        println!("名称: {:<30}版本: {:<30}", name, version);
        let environments = environment::global().get_environments(name);
        if environments.is_some() {
            let environments = environments.unwrap();
            for (k, v) in environments.iter() {
                println!("\t全局ENV: {}={}", k, v);
            }
        }
    }
}

