use clap::{Arg, ArgAction, ArgMatches, Command};

use crate::environment::environment;

pub(super) const NAME: &'static str = "uns";

pub(super) fn new() -> Command {
    let mut command = Command::new(NAME).about("移除环境变量");
    command = command.arg(Arg::new("name").required(true).help("工具名称"));
    command = command.arg(Arg::new("keys").action(ArgAction::Append).required(true).help("变量名称，样例: KEY [KEY] [KEY]"));

    command
}

pub(super) fn action(args: &ArgMatches) {
    let name = args.get_one::<String>("name").unwrap();
    let kvs: Vec<&String> = args.get_many::<String>("keys").unwrap().into_iter().collect();

    for key in kvs.into_iter() {
        let result = environment::global_mut().unset_environment(name.as_str(), key);
        if result.is_err() {
            println!("移除环境变量失败: {}", result.unwrap_err());
        }
    }
}