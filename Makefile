.PHONY: build_mac
build_mac: build_mac_apple build_mac_intel

.PHONY: build_linux
build_linux: build_linux_aarch64 build_linux_x64

.PHONY: build_mac_apple
build_mac_apple:
	rustup target add aarch64-apple-darwin
	cargo build --target aarch64-apple-darwin --all -r

.PHONY: build_mac_intel
build_mac_intel:
	rustup target add x86_64-apple-darwin
	cargo build --target x86_64-apple-darwin --all -r

.PHONY: build_linux_aarch64
build_linux_aarch64:
	rustup target add aarch64-unknown-linux-musl
	cargo build --target aarch64-unknown-linux-musl --all -r

.PHONY: build_linux_x64
build_linux_x64:
	rustup target add x86_64-unknown-linux-musl
	cargo build --target x86_64-unknown-linux-musl --all -r
